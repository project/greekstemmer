# Important

The file `src/GreekStemmer.php` is intentionally in ISO-8859-7 encoding.

# TODO

Though seemingly in a full-ready state, this module still has two goals we could achieve to make things easier for anyone willing to try it:

1. Turn `src/GreekStemmer.php` to UTF-8 encoding to match Drupal's coding standards.
2. [TBD] Develop the solution for Greek in [wamania/php-stemmer](https://github.com/wamania/php-stemmer) or equivalent library and mark it as a dependency of this module. This would also benefit people outside Drupal - though it would add an extra dependency to this module which might add extra fuss in the future.

# Copyright

* Copyright (c) 2009 Vassilis Spiliopoulos (http://www.psychfamily.gr),  Pantelis Nasikas under GNU General Public License Version 3
* Updated for Drupal 6, 7 and Drupal 8 by Yannis Karampelas (info@netstudio.gr) in 2011 and 2017 respectively.
* Updated for Drupal 9 and 10 by Panagiotis Moutsopoulos (vensires) while working for E-sepia Web Innovation.
* This is a port of the php implementation of Spyros Saroukos into Drupal CMS. Spyros Saroukos implementation was based on the work of Panos Kyriakakis (http://www.salix.gr) and Georgios Ntais (Georgios.Ntais@eurodyn.com). Georgios firstly developed the stemmer's javascript implementation for his master thesis at Royal Institute of Technology [KTH], Stockholm Sweden: http://www.dsv.su.se/~hercules/papers/Ntais_greek_stemmer_thesis_final.pdf.
