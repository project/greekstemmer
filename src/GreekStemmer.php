<?php

namespace Drupal\greekstemmer;

/**
 * The encoding of this file is iso-8859-7 instead of UTF-8 on purpose!!!!!!!
 */
class GreekStemmer {

  /**
   * Process search keyword.
   */
  public static function searchPreprocess($text) {
    $en_changed = FALSE;
    if (mb_check_encoding($text, "UTF-8")) {
      $en_changed = TRUE;
      $text = mb_convert_encoding($text, "ISO-8859-7", "UTF-8");
    }
    $words = preg_split('/[^(a-zA-Z�����������������������������������������������������������ࢸ�������\d)]+/', str_replace("'", '', $text), -1, PREG_SPLIT_DELIM_CAPTURE);

    // Process each word.
    foreach ($words as $k => $word) {
      $stemmed_array = self::stemWord($word);
      $words[$k] = $stemmed_array[0];
    }

    // Put it all back together.
    $to_return_string = implode(" ", $words);
    if ($en_changed == TRUE) {
      $to_return_string = mb_convert_encoding($to_return_string, "UTF-8", "ISO-8859-7");
    }
    return $to_return_string;
  }

  /**
   * Returns the stem.
   *
   * @param string $w
   *   A greek word to get its stem.
   *
   * @return array
   *   Returns the stem as a string. Check self::returnStem() for the returned
   *   value. In most cases, you would only require key 0 from the returned
   *   array.
   *
   * @see self::returnStem()
   */
  public static function stemWord($w) {
    // This is the number of rules examined for debugging/testing purposes.
    $numberOfRulesExamined = 0;

    // It is better to convert the input into iso-8859-7 in case it is in utf-8.
    // This way we don't have any problems with length counting etc.
    $encoding_changed = FALSE;
    // 1 for changed case in that position, 2 especially for �
    $w_CASE = [strlen($w)];

    // First we must find all letters that are not in Upper case and store their
    // position.
    $unacceptedLetters = [
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
    ];
    $acceptedLetters = [
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
    ];

    for ($k = 0; $k <= 32; $k = $k + 1) {
      for ($i = 0; $i <= strlen($w) - 1; $i++) {
        if ($w[$i] === $unacceptedLetters[$k]) {
          if ($w[$i] === "�") {
            $w[$i] = "�";
            $w_CASE[$i] = 2;
          }
          else {
            $w[$i] = $acceptedLetters[$k];
            $w_CASE[$i] = "1";
          }
        }
      }
    }
    // Stop-word removal.
    $numberOfRulesExamined++;
    $stop_words = '/^(���|���|���|���|���|���|���|��|���|���|��|���|���|�����|�����|�������|���|�������|�������|�������|������|���������|�������|����������|������|���|���|���|���|�����|������|���|��|���|�������|���������|����|����|��������|�����|���|�������|������|��|��������|���|�����|���|���|�����|���|���|������|���|���|���|���|���|��|���|���|���|���|���|���|���|���|�����|���|������|����|��|���|���|���|���|���|���|���|���|���|���|���|���|����������|���|���|���|���|���|���|���|���|��|���|���|���|���||��|���|��|���|���|���|���|���|�����|���|���|���|�������|���|���|���|����|��|��|���|���|���|���|��|�����|������|���|��|���|�������|������|���|�����|���|���|���|���|���|���|���|��|���|���|���|���|��|������|����|�����|�������|�����|���|�����|�������|�����|����|����|������|�����|������|����|�����|��|����|���|���|���|��|�������|�����|����������|���|��|������|���|������|���|���|�����|������|������|���|���|������|��������|���|��|����|���|�����|�����|���|����|���|���|���|����|��|���|����|�����|�������|���|������|���|���|����|�����|�����|�����|�����|������|�����|�����|���|���|���|���|���|���|���|��|�|���|���|���|���|���|���|������|�����|���|����|�����|����|�����|��|��|���|���|�|��|���|���|�����|��|��|���|���|���|���|���|���|���|���|��|���|���|�����|���|���|����|���|����|���|���|���|���|���|����|������|�������|�����|���|���|������|�����|�����|���|����|����|������|�������|����|���|���|���|���|���|��|���|������|���|���|���|�����|���|���|�����|���|���|���|������|��|���|��|���|���|���|���|������|���������|����|��������|���|���|����|�����|������|���|��|���|��|����|������|�������|������|���|���|���|���|��|��������|���|�����|���|�����|��������|������|���|���|����|���|����|������|�����|��|����|���|�����|����|��|���|���|���|���|���|�����|��������|������|��������|���|���|���|������|�������|������|�����|���|��|���|���|��|���|���|���|��|���|���|���|���|��|���|���|���|���|���|���|���|���|���|���|���|�����|����|�������|���|��|�|��|���|���|��|���|���|��|���|���|���|�������|������|�������|����������|����|��|���|���|���|���|���|���|�����������|������������|������������|����������|������������|�����������|������������|������������|������������|�������������|������������|�����������|����|����������|����|���|���|���|���|���|���|���|���������|���|����������|���������|����������|����������|���������|����������|����������|����������|����������|�����������|����������|����|���|���|���������|��|����|���|���|����|���|���|���|���|���|���|���|��|����|���|�������|������|������|���|���|����|���|���|����|����|�������|�����|������|���|��|���|�������|���|���|����|���|���|����|�����|����|���|��|���|���|���|����|���|����|���|�����|�������|������|���|����|���|�����������|���������|��������|����|������|�������|�������|���������|���|���|���|���|��|��|���|���|��|���|���|���|��|���|���|���|���|���|���|��|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|��|����|���|���|���|���|���|������|��|���|����|���|���|���|���|���|���|���|���|���|���|���|���|����|����|����|���|����|����|�����|����|��|���������|���|������|�������|�������|������|�����|��|����|���|���|���|���|����|������|��|���|���|������|�������|���|���|���|��|���|���|���|��|���|���|������|������|���|���|��|���|���|���|���|���|���|���?�|����|�����|����|�����|�����|����|�����|�����|�����|�����|������|����|���|����������|�����������|����|��|���|���|�����|��|���|����|���|���|���|���|���|���|���|���|��|����|���|�����|������|������|���|���|��|���|���|���|���|���|���|���|���|�����|���|��|���|���|���|���|���|��|���|���|���|���|��|���|����|��|���|���|���|���|��|���|���|���|����|�����|�������|���|����|��|���|�|��|���|���|���|���|���|���|��|��|���|���|��|����|���|������|�����|����|������|���|��|���)$/';

    if (preg_match($stop_words, $w)) {
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step1list is used in Step 1. 41 stems.
    $step1list = [];
    $step1list["�����"] = "��";
    $step1list["������"] = "��";
    $step1list["������"] = "��";
    $step1list["������"] = "���";
    $step1list["�������"] = "���";
    $step1list["�������"] = "���";
    $step1list["�������"] = "���";
    $step1list["������"] = "���";
    $step1list["�������"] = "���";
    $step1list["������"] = "��";
    $step1list["�����"] = "��";
    $step1list["������"] = "��";
    $step1list["�������"] = "����";
    $step1list["��������"] = "����";
    $step1list["��������"] = "����";
    $step1list["�����"] = "���";
    $step1list["�������"] = "���";
    $step1list["������"] = "���";
    $step1list["�������"] = "���";
    $step1list["�����"] = "���";
    $step1list["�������"] = "���";
    // Added by Spyros . Also at $re in step1.
    $step1list["������"] = "���";
    $step1list["������"] = "���";
    $step1list["�������"] = "���";
    $step1list["�����"] = "���";
    $step1list["�������"] = "���";
    $step1list["������"] = "���";
    $step1list["�������"] = "���";
    $step1list["���"] = "��";
    $step1list["�����"] = "��";
    $step1list["����"] = "��";
    $step1list["�����"] = "��";
    $step1list["��������"] = "������";
    $step1list["����������"] = "������";
    $step1list["���������"] = "������";
    $step1list["����������"] = "������";
    $step1list["�������"] = "�����";
    $step1list["���������"] = "�����";
    $step1list["��������"] = "�����";
    $step1list["���������"] = "�����";

    // Vowel.
    $v = '(�|�|�|�|�|�|�)';
    // Vowel without "�".
    $v2 = '(�|�|�|�|�|�)';

    $test1 = TRUE;

    // Step S1. 14 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|����|���|�����|�����|����|�����|���|�����|����|������|�����|�����|������)$/';
    $exceptS1 = '/^(������|����|���|������|��|������|����|�������|����)$/';
    $exceptS2 = '/^(����|����|�����|���|������|����|�|������|���|������|������|���|�|���|���|�|��|���|�����|�|�|�������)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= 'I';
      }
      if (preg_match($exceptS2, $w)) {
        $w .= 'I�';
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S2. 7 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|������|�����|�������|�������|������|�������)$/';
    $exceptS1 = '/^(��|��|��|��|��|��|�|�)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= '��';
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S3. 7 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|����|���|�����|�����|����|�����)$/';
    $exceptS1 = '/^(������|����|����|���|������|���|������|���|������|����|�������|����|���|�������|������|������|������|������|����|��|������)$/';
    $exceptS2 = '/^(��|��|��|���������|���|��������|���|��|�|�|������|���|���)$/';

    if ($w === "���") {
      $w = "��";
      // @todo Shouldn't we call self::returnStem() here too?
      return [$w, 0];
    }
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= '�';
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S4. 7 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|�����|����|������|�����|�����|������)$/';
    $exceptS1 = '/^(������|����|���|������|���|������|���|������|����|�������|����|���|�������|������|������|������|������|����|��|������)$/';

    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= '�';
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S5. 11 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|�����|����|����|�����|�����|������|����|�����|����|�����)$/';
    $exceptS1 = '/^(�|�|��|��|��|��|��|��|��|��|��|��|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���|���)$/';
    $exceptS2 = '/^(����|�������|���|��|������|���|���)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= '���';
      }
      if (preg_match($exceptS2, $w)) {
        $w .= '�';
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S6. 6 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|�����|�����|�����|������|�����)$/';
    $exceptS1 = '/^(��������|������|�������|�����|��������|�������|�����)$/';
    $exceptS2 = '/^(��|������|�������|�����|������)$/';
    $exceptS3 = '/^(����|��������)$/';
    $exceptS4 = '/^(����������|��������|�������)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem;
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w = str_replace('��', "", $w);
      }
      if (preg_match($exceptS2, $w)) {
        $w .= "���";
      }
      if (preg_match($exceptS3, $w)) {
        $w .= "�";
      }
      if (preg_match($exceptS4, $w)) {
        $w = str_replace('��', "", $w);
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S7. 4 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|������|������|�������)$/';
    $exceptS1 = '/^(�|�)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem;
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= "A���";
      }

      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S8. 8 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|����|����|�����|�����|�����|�����|������)$/';
    $exceptS1 = '/^(����|����|��|����|���|���|����|����|���|�����|������|��|�|��|�|��|���|����|��|����|�|�����|�����|����|����|�|���|������|����|���|����|�|��|���������)$/';
    $exceptS2 = '/^(�|���|����|��|�|�������|����|���|������|���|�����|�|��|���|������)$/';
    // For words like ��������������, ������������ etc,
    $exceptS3 = '/(���)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem;
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w .= "��";
      }
      if (preg_match($exceptS2, $w)) {
        $w .= "���";
      }
      if (preg_match($exceptS3, $w)) {
        $w .= "���";
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S9. 3 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|����|�����)$/';
    $exceptS1 = '/^(����|��|���|���)$/';
    $exceptS2 = '/(�|�����)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem;
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w = $w . "��";
      }
      if (preg_match($exceptS2, $w)) {
        $w = $w . "��";
      }
      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step S10. 4 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|�����|����|����)$/';
    $exceptS1 = '/^(�|��|���|�|�����|���|����)$/';
    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem;
      $test1 = FALSE;
      if (preg_match($exceptS1, $w)) {
        $w = $w . "���";
      }

      return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
    }

    // Step1.
    $numberOfRulesExamined++;
    $re = '/(.*)(�����|������|������|������|�������|�������|�������|������|�������|������|�����|������|�������|��������|��������|�����|�������|������|�������|�����|�������|������|������|�������|�����|�������|������|�������|���|�����|����|�����|��������|����������|���������|����������|�������|���������|��������|���������)$/';

    if (preg_match($re, $w, $match)) {
      [, $stem, $suffix] = $match;
      $w = $stem . $step1list[$suffix];
      $test1 = FALSE;
    }

    // Step 2a. 2 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|����)$/';
    if (preg_match($re, $w, $match)) {
      $stem = $match[1];
      $w = $stem;
      $re = '/(��|���|���|�����|�����|�����|�����|���|���|�����)$/';
      if (!preg_match($re, $w)) {
        $w = $w . "��";
      }
    }

    // Step 2b. 2 stems.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $exept2 = '/(��|��|���|��|���|���|�����|���)$/';
      if (preg_match($exept2, $w)) {
        $w = $w . '��';
      }
    }

    // Step 2c.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|�����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;

      $exept3 = '/(���|������|�����|���|����|��|�|��|��|���|����|��|��|����|��)$/';
      if (preg_match($exept3, $w)) {
        $w = $w . '���';
      }
    }

    // Step 2d.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|���)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept4 = '/^(�|�|��|���|�|�|��|���)$/';
      if (preg_match($exept4, $w)) {
        $w = $w . '�';
      }
    }

    // Step 3.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(��|���|���)$/';
    if (preg_match($re, $w, $fp)) {
      $stem = $fp[1];
      $w = $stem;
      $re = '/' . $v . '$/';
      $test1 = FALSE;
      if (preg_match($re, $w)) {
        $w = $stem . '�';
      }
    }

    // Step 4.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|���|����|����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;

      $test1 = FALSE;
      $re = '/' . $v . '$/';
      $exept5 = '/^(��|��|���|����|�������|��|����|�����|���|����|���|����|����|������|�����|����|����|�������|����|����|���|���|�������|����|����|������|������|�������|������|����|�����|����|����|�����|�����|���)$/';
      if (preg_match($re, $w) || preg_match($exept5, $w)) {
        $w = $w . '��';
      }
    }

    // Step 5a.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���)$/';
    $re2 = '/^(.+?)(�����|�����|������|�����|�������)$/';
    if ($w == "�����") {
      $w = "����";
    }

    if (preg_match($re2, $w)) {
      preg_match($re2, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;
    }
    $numberOfRulesExamined++;
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept6 = '/^(����|����|����|�����|����|���|���|���|����|���|���|�)$/';
      if (preg_match($exept6, $w)) {
        $w = $w . "��";
      }
    }

    // Step 5b.
    $numberOfRulesExamined++;
    $re2 = '/^(.+?)(���)$/';
    $re3 = '/^(.+?)(�����|�����|������|�������|������|��������|������|�����|�������|�����|�������)$/';

    if (preg_match($re3, $w)) {
      preg_match($re3, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $re3 = '/^(��|��)$/';
      if (preg_match($re3, $w)) {
        $w = $w . "����";
      }
    }
    $numberOfRulesExamined++;
    if (preg_match($re2, $w)) {
      preg_match($re2, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $re2 = '/' . $v2 . '$/';
      $exept7 = '/^(�����|�����|�����|�|�������|�|�������|������|������|�����|������|�|��������|�|���|�|�����|��|�����|������|��������|�����|�������|���|�����|����|��������|�|������|��|���|���|���|���|����|��������|���|���|������|�|����|��|����|���|���|������|�����|���|���|��|����|����|����|�|��|����|�����|����|����|�����|����|����|������|���|����|�������|������|������|����|����|�����|���|�����������|�������|����|�������|���|�����������|�����������|����|��������|��������|������|�������|�����|������|����|�������|�������|����|���|���|������|������|���������|�������)$/';
      if (preg_match($re2, $w) || preg_match($exept7, $w)) {
        $w = $w . "��";
      }
    }

    // Step 5c.
    $numberOfRulesExamined++;
    $re3 = '/^(.+?)(���)$/';
    $re4 = '/^(.+?)(�����)$/';

    if (preg_match($re4, $w)) {
      preg_match($re4, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;
    }
    $numberOfRulesExamined++;
    if (preg_match($re3, $w)) {
      preg_match($re3, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $re3 = '/' . $v2 . '$/';
      $exept8 = '/(��|���|���|���|����|��|���|���|���|�����|���|���|���|��|���|���|����|���|����|���|���|��|���|���|���|���|���|���|���|���|����)$/';
      $exept9 = '/^(����|���|����|���|��|��|��|���|�����|���|��|���|����|���|���|�������|����|����|����|���|�|�|��|����|�)$/';

      if (preg_match($re3, $w) || preg_match($exept8, $w) || preg_match($exept9, $w)) {
        $w = $w . "��";
      }
    }

    // Step 5d.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|�����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept10 = '/^(���)$/';
      $exept11 = '/(���)$/';
      if (preg_match($exept10, $w)) {
        $w = $w . "���";
      }
      if (preg_match($exept11, $w)) {
        $w = $w . "���";
      }
    }

    // Step 5e.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(������|�������)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept11 = '/^(��)$/';
      if (preg_match($exept11, $w)) {
        $w = $w . "�����";
      }
    }

    // Step 5f.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����)$/';
    $re2 = '/^(.+?)(�����)$/';

    if (preg_match($re2, $w)) {
      preg_match($re2, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $re2 = '/^(�|��|����|�����|������|�������)$/';
      if (preg_match($re2, $w)) {
        $w = $w . "����";
      }
    }
    $numberOfRulesExamined++;
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept12 = '/^(��|��|�����|�|�|�|�������|��|���|���)$/';
      if (preg_match($exept12, $w)) {
        $w = $w . "���";
      }
    }

    // Step 5g.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|����|���)$/';
    $re2 = '/^(.+?)(�����|������|�����)$/';

    if (preg_match($re2, $w)) {
      preg_match($re2, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;
    }
    $numberOfRulesExamined++;
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept13 = '/(����|�����|����|��|��|���)$/';
      $exept14 = '/^(����|�|���������|�����|����|)$/';
      if (preg_match($exept13, $w) || preg_match($exept14, $w)) {
        $w .= "��";
      }
    }

    // Step 5h.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|�����|����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept15 = '/^(������|���|���|�����|����|�����|������|���|�|���|�|�|���|�����|�������|��|���|����|������|��������|��|��������|�������|���|���)$/';
      $exept16 = '/(�����|����|������|����|������|����|�����|���|���|���|��|����)$/';
      if (preg_match($exept15, $w) || preg_match($exept16, $w)) {
        $w .= "���";
      }
    }

    // Step 5i.
    $re = '/^(.+?)(���|����|���)$/';
    $numberOfRulesExamined++;
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept17 = '/^(���|������)$/';
      $exept20 = '/(����)$/';
      $exept18 = '/^(�����|�����|����|����|�|���|��|����|������|�����|����|�����|����|������|������|���|����|�����|����|����|�����|��������|����|����|�|����|���|����|������|����|����|�����|����|��|����|��������|�������|�|���|�����|���|�|��|�)$/';
      $exept19 = '/(��|���|����|��|��|��|��|��|���|����)$/';

      if ((preg_match($exept18, $w) || preg_match($exept19, $w))
        && !(preg_match($exept17, $w) || preg_match($exept20, $w))) {
        $w .= "��";
      }
    }

    // Step 5j.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(���|����|���)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept21 = '/^(�|������|�������|������|�������|�����)$/';
      if (preg_match($exept21, $w)) {
        $w .= "��";
      }
    }

    // Step 5k.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept22 = '/^(���|��|���|��|���|�����|�����|����|�������|������)$/';
      if (preg_match($exept22, $w)) {
        $w .= "���";
      }
    }

    // Step 5l.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|������|������)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept23 = '/^(�|�|���|�����������|���������|����)$/';
      if (preg_match($exept23, $w)) {
        $w .= "���";
      }
    }

    // Step 5l.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|������|������)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
      $test1 = FALSE;

      $exept24 = '/^(��������|�|�|������|��|��������|�����)$/';
      if (preg_match($exept24, $w)) {
        $w .= "���";
      }
    }

    // Step 6.
    $numberOfRulesExamined++;
    $re = '/^(.+?)(����|�����|�����)$/';
    $re2 = '/^(.+?)(�|�����|����|���|����|��|��|����|����|��|�|��|���|����|����|��|����|�|�����|�������|�����|�����|�������|��������|������|�������|������|���������|��������|�������|������|�������|�����|�����|��������|�������|�������|�|����|����|����|�����|������|�������|������|�����|���|�����|����|��|����|�����|����|����|�����|���|�|��|����|�������|�����|������|�����|�����|��������|��|�������|������|�����|������|����|��|�����|�������|���|������|������|���|�����|������|�|��|�|��)$/';
    if (preg_match($re, $w, $match)) {
      $stem = $match[1];
      $w = $stem . "��";
    }
    $numberOfRulesExamined++;
    if (preg_match($re2, $w) && $test1) {
      preg_match($re2, $w, $match);
      $stem = $match[1];
      $w = $stem;
    }

    // Step 7 (����������)
    $numberOfRulesExamined++;
    $re = '/^(.+?)(�����|�����|����|����|����|����|����|����)$/';
    if (preg_match($re, $w)) {
      preg_match($re, $w, $match);
      $stem = $match[1];
      $w = $stem;
    }

    return self::returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined);
  }

  /**
   * Returns the stem word.
   *
   * @param string $w
   *   The stem.
   * @param array $w_CASE
   *   An array with multiple values "1" or "2".
   * @param bool $encoding_changed
   *   Set whether the encoding of the text was changed.
   * @param int $numberOfRulesExamined
   *   The number of rules examind (for debugging purposes).
   *
   * @return array
   *   Returns an array with the following values:
   *    - key 0: The stem
   *    - key 1: The number of executed rules (for debugging/testing purposes).
   */
  public static function returnStem($w, $w_CASE, $encoding_changed, $numberOfRulesExamined) {
    // Convert case back to initial by reading $w_CASE.
    $unacceptedLetters = [
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
    ];
    $acceptedLetters = [
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
      "�",
    ];
    for ($i = 0; $i <= strlen($w) - 1; $i++) {
      if (@$w_CASE[$i] == 1) {
        for ($k = 0; $k <= 32; ++$k) {
          if ($w[$i] === $acceptedLetters[$k]) {
            $w[$i] = $unacceptedLetters[$k];
          }
        }
      }
      else {
        if (@$w_CASE[$i] == 2) {
          $w[$i] = "�";
        }
      }
    }

    $returnResults = [];
    $returnResults[0] = $w;
    $returnResults[1] = $numberOfRulesExamined;
    return $returnResults;
  }

}
